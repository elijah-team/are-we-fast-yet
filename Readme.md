This is a version of the "Are We Fast Yet?" benchmark suite
migrated to Elijah.

The derived source was downloaded on 2021-08-17 from
https://github.com/rochus-keller/Oberon
commit be0b6451470068e56356785a5a3d4d5a3b48493d

---------

This is a version of the "Are We Fast Yet?" benchmark suite
migrated to Oberon+.

The original source code was downloaded on 2020-08-08 from 
https://github.com/smarr/are-we-fast-yet/
commit 770c6649ed8e by 2020-04-03

See [ORIGINAL_README.md](ORIGINAL_README.md), [AUTORS.md](AUTORS.md) and [LICENSE.md](LICENSE.md) for
more information. 
